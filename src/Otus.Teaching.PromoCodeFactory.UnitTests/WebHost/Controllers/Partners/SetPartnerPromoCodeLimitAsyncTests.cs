﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFoundResult()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsBlocked_ReturnsBadRequestObjectResult()
        {
            // Arrange
            //Инициализация фабричным методом
            Partner partner = CreateBasePartner();
            partner.IsActive = false;

            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            Assert.Equal("Данный партнер не активен", ((BadRequestObjectResult)result).Value);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerLimitSetUpdateNumberIssuedPromoCodes_CreatedAtActionResult()
        {
            //Arrange
            //Инициализация строителем
            Partner partner = new PartnerBuilder()
            .WithCreateBasePartner()
            .WithNumberIssuedPromoCodes(10)
            .Build();

            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 50,
                EndDate = new DateTime(2022, 12, 31)
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var redirectToActionResult = Assert.IsType<CreatedAtActionResult>(result);
            _partnersRepositoryMock.Verify(r => r.UpdateAsync(partner));
            Assert.Equal("GetPartnerLimitAsync", redirectToActionResult.ActionName);
            Assert.Equal(0, partner.NumberIssuedPromoCodes);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, 
        /// если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerLimitSetUpdateNumberIssuedPromoCodesLimitWhithCancelDate_CreatedAtActionResult()
        {
            //Arrange

            //Инициализация autoFixture
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => autoFixture.Behaviors.Remove(b));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var partner = autoFixture.Build<Partner>()
                .With(p => p.NumberIssuedPromoCodes, 10)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> 
                    { 
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            CancelDate = DateTime.Now.AddMonths(-1),
                            Limit = 100
                        }
                    }
                )
                .Create();

            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 50,
                EndDate = new DateTime(2022, 12, 31)
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var redirectToActionResult = Assert.IsType<CreatedAtActionResult>(result);
            _partnersRepositoryMock.Verify(r => r.UpdateAsync(partner));
            Assert.Equal("GetPartnerLimitAsync", redirectToActionResult.ActionName);
            Assert.Equal(10, partner.NumberIssuedPromoCodes);
        }
        
        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ClosePreviousLimitOnAddNewLimit_CreatedAtActionResult()
        {
            //Arrange
            //Инициализация строителем
            Partner partner = new PartnerBuilder()
            .WithCreateBasePartner()
            .WithNumberIssuedPromoCodes(10)
            .Build();

            var partnerId = partner.Id;
            var limitId = partner.PartnerLimits.FirstOrDefault().Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 50,
                EndDate = new DateTime(2022, 12, 31)
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var redirectToActionResult = Assert.IsType<CreatedAtActionResult>(result);
            _partnersRepositoryMock.Verify(r => r.UpdateAsync(partner));
            Assert.Equal("GetPartnerLimitAsync", redirectToActionResult.ActionName);
            Assert.NotNull(partner.PartnerLimits.FirstOrDefault(x => x.Id == limitId).CancelDate);
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitMustBeMoreThanZero_CreatedAtActionResult()
        {
            //Arrange
            //Инициализация строителем
            Partner partner = new PartnerBuilder()
            .WithCreateBasePartner()
            .Build();

            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 0,
                EndDate = new DateTime(2022, 12, 31)
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            Assert.Equal("Лимит должен быть больше 0", ((BadRequestObjectResult)result).Value);
        }
    }
}