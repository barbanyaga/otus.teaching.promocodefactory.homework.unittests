﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private bool _isActive = true;
        private ICollection<PartnerPromoCodeLimit> _partnerLimits;
        private Partner _partner;
        private int _numberIssuedPromoCodes;

        public PartnerBuilder()
        {
            
        }

        public PartnerBuilder WithCreateBasePartner()
        {
            _partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2022, 1, 1),
                        EndDate = new DateTime(2022, 7, 31),
                        Limit = 100
                    }
                }
            };
            return this;
        }

        public PartnerBuilder WithIsActive(bool IsActive)
        {
            _isActive = IsActive;
            return this;
        }

        public PartnerBuilder WithLimit(PartnerPromoCodeLimit Limit)
        {
            _partnerLimits = new List<PartnerPromoCodeLimit>
            {
                Limit
            };
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int NumberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = NumberIssuedPromoCodes;
            return this;
        }

        public Partner Build()
        {
            if(_partner != null)
            {
                _partner.IsActive = _isActive;
                if(_partnerLimits != null)
                    _partner.PartnerLimits = _partnerLimits;
                _partner.NumberIssuedPromoCodes = _numberIssuedPromoCodes;
                return _partner;
            }
            else
            {
                return new Partner
                {
                    IsActive = _isActive,
                    PartnerLimits = _partnerLimits
                };
            }
        }
    }
}
