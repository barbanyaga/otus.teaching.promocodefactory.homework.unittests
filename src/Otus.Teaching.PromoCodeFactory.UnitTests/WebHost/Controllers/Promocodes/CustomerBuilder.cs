﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Promocodes
{
    public class CustomerBuilder
    {
        private Customer _customer;

        public CustomerBuilder()
        {
            
        }

        public CustomerBuilder WhithCreateBaseCustomer()
        {
            var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
            _customer = new Customer
            {
                Id = customerId,
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
                Preferences = new List<CustomerPreference>()
                {
                    new CustomerPreference()
                    {
                        CustomerId = customerId,
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    }
                }
            };

            return this;
        }

        public Customer Build()
        {
            return _customer;
        }
    }
}
