﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Promocodes
{
    public class PromoCodeBuilder
    {
        private PromoCode _promoCode;

        public PromoCodeBuilder()
        {

        }

        public PromoCodeBuilder WithCreateBasePromoCode()
        {
            _promoCode = new PromoCode()
            {
                Id = Guid.Parse("a8e9177e-bf1b-4b09-b665-c786d0752b46"),
                Code = "TESTCODE123",
                ServiceInfo = "Test Service",
                BeginDate = DateTime.Now.AddMonths(-1),
                EndDate = DateTime.Now.AddMonths(5),
                Preference = new Preference()
                {
                    Name = "Семья"
                }
            };
            return this;
        }

        public PromoCode Build()
        {
            return _promoCode;
        }
    }
}
