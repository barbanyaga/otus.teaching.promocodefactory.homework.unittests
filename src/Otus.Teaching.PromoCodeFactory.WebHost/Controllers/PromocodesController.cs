﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository,
            IRepository<Employee> employeeRepository, IRepository<Customer> customerRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _employeeRepository = employeeRepository;
            _customerRepository = customerRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var response = preferences.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить промокод по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodeByIdAsync(Guid Id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(Id);

            var response = new PromoCodeShortResponse()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = promoCode.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo
            };

            return Ok(response);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<List<CustomerShortResponse>>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //Добавить промокод
            var preferences = await _preferencesRepository.GetAllAsync();
            var employee = await _employeeRepository.GetByIdAsync(request.PartnerManagerId);

            PromoCode promoCode = new PromoCode()
            {
                Code = request.PromoCode,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName
            };

            promoCode.Preference = preferences.Where(p => p.Name == request.Preference).FirstOrDefault();
            promoCode.PartnerManager = employee;

            await _promoCodesRepository.AddAsync(promoCode);

            //Вернуть список клиентов с промокодами по предпочтениям
            var allCustomers = await _customerRepository.GetAllAsync();
            var customers = allCustomers.Where(c => c.Id == c.Preferences.FirstOrDefault(p => p.PreferenceId == promoCode.Preference.Id).CustomerId);
            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return Ok(response);
        }
    }
}